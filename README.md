# Bookmark Manager

Application that manages bookmarks.

1. This application is using Angular 11 . With NGRX for managing the state and Angular Material for the User Interface.
2. Each Bookmark has ID, Name, URL and Group.
3. Bookmarks are group by "group" property.
4. This application can show bookmark by clicking the View Bookmark Icon, it can also add, update and delete bookmark.

### Run the application

Run `npm install ` then `npm start`.

### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

### Prerequisites
- NodeJS Version 13.9.0

- NPM Version 6.13.7

- Use port 8181

## Home Page
![HomePage](images/HomePage.png)


## View Bookmark Details

![ViewBookmark](images/ViewBookmark.png)

![ViewBookmarkDetails](images/ViewBookmarkDetails.png)

## Add new Bookmark

![AddBookmark](images/AddBookmark.PNG)

![AddBookmarkDetails](images/AddBookmarkDetails.png)

## Update Bookmark Details

![UpdateBookmark](images/UpdateBookmark.PNG)

![UpdateBookmarkDetails](images/UpdateBookmarkDetails.png)

## Delete Bookmark

![DeleteBookmark](images/DeleteBookmark.PNG)

![DeleteBookmarkDetails](images/DeleteBookmarkDetails.png)
