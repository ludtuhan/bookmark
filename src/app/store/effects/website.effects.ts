import { Actions, ofType, Effect } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import {switchMap, map} from 'rxjs/operators';
import { EWebsiteActions, LoadWebsiteInit, LoadWebsiteDone } from '../actions/website.actions';
import { WebsiteService } from '../../services/Website.service';

@Injectable({providedIn: 'root'})
export class WebsiteEffects {

    constructor(private actions$: Actions, private websiteService: WebsiteService){ }

    @Effect()
    public loadWebsites$ = this.actions$.pipe(
        ofType(EWebsiteActions.LOAD_WEBSITE_INIT),
        switchMap((action: LoadWebsiteInit) =>
            this.websiteService.getWebsites().pipe(
                map(websites => new LoadWebsiteDone(websites))
            )
        )
    );
}
