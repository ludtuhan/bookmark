import { Action } from '@ngrx/store';

import { Website, Websites } from '../state/website.state';

export enum EWebsiteActions {
    GET_WEBSITES = '[Website] Get Websites',
    ADD_WEBSITE = '[Website] Add Website',
    UPDATE_WEBSITE = '[Website] Edit Website',
    DELETE_WEBSITE = '[Website] Delete Website',
    LOAD_WEBSITE_INIT = '[Website] Load Website init',
    LOAD_WEBSITE_DONE = '[Website] Load Website done',
    RESTORE_WEBSITES = '[Website] Restore Websites to current state'
}

export class GetWebsites implements Action {
    readonly type = EWebsiteActions.GET_WEBSITES;

    constructor(public payload: any) {
        console.log('ACTION ' + EWebsiteActions.GET_WEBSITES);
    }
}

export class AddWebsite implements Action {
    readonly type = EWebsiteActions.ADD_WEBSITE;

    constructor(public payload: Website) {
        console.log('ACTION ' + EWebsiteActions.ADD_WEBSITE);
    }
}

export class UpdateWebsite implements Action {
  readonly type = EWebsiteActions.UPDATE_WEBSITE;

  constructor(public payload: Website) {
      console.log('ACTION ' + EWebsiteActions.UPDATE_WEBSITE);
  }
}

export class DeleteWebsite implements Action {
    readonly type = EWebsiteActions.DELETE_WEBSITE;

    constructor(public payload: Website) {
        console.log('ACTION ' + EWebsiteActions.DELETE_WEBSITE);
    }
}

export class LoadWebsiteInit implements Action {
    readonly type = EWebsiteActions.LOAD_WEBSITE_INIT;

    constructor(public payload: any){
        console.log('ACTION ' + EWebsiteActions.LOAD_WEBSITE_DONE);
    }
}

export class LoadWebsiteDone implements Action {
    readonly type = EWebsiteActions.LOAD_WEBSITE_DONE;

    constructor(public payload: Websites) {
        console.log('Websites - ' + payload);
    }
}

export class RestoreWebsites implements Action {
  readonly type = EWebsiteActions.RESTORE_WEBSITES;

  constructor(public payload: any) {
      console.log('ACTION ' + EWebsiteActions.RESTORE_WEBSITES);
  }
}

export type WebsiteActions = GetWebsites | AddWebsite | UpdateWebsite | DeleteWebsite | LoadWebsiteInit | LoadWebsiteDone | RestoreWebsites;
