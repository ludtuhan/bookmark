import { EWebsiteActions, WebsiteActions } from '../actions/website.actions';
import { Websites, initialWebsitesState } from '../state/website.state';

export function websiteReducer(state = initialWebsitesState, action: WebsiteActions): Websites {
	switch (action.type) {
        case EWebsiteActions.GET_WEBSITES: {
            return  {...state};
        }
        case EWebsiteActions.ADD_WEBSITE: {
            return  { websites: sort(state.websites.concat(action.payload), 'name')};
        }
        case EWebsiteActions.UPDATE_WEBSITE: {
	        return  { websites: sort(state.websites.filter(website => website.id !== action.payload.id).concat(action.payload), 'name')};
        }
        case EWebsiteActions.DELETE_WEBSITE: {
            return  { websites: sort(state.websites.filter(website => website.id !== action.payload.id), 'name')};
        }
        case EWebsiteActions.LOAD_WEBSITE_DONE: {
            return  { websites: sort(state.websites.concat(action.payload.websites), 'name')};
        }
        default: {
            return {
                ...state
            };
        }
	}
}

function sort(websites, property) {
	return websites.sort((a,b) => {
        if(a[property] < b[property]) {
            return -1;
        } else {
            return (a[property] > b[property]) ? 1 : 0
        }
    });
}