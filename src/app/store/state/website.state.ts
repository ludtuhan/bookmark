export interface Website {
	id: number,
	name: string,
	url: string,
	group:string
	
}

export interface Websites {
	websites: Website[];
}

export const initialWebsitesState: Websites = {
	websites: []
}