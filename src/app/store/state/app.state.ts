import { ActionReducerMap } from '@ngrx/store';
import { createSelector } from '@ngrx/store';
import * as websiteState from '../state/website.state';
import { websiteReducer } from '../reducers/website.reducers';
import { WebsiteEffects } from '../effects/website.effects';

export interface AppState {
    websites: websiteState.Websites;
}

export const initialState: AppState = {
    websites: websiteState.initialWebsitesState
};

export const reducers: ActionReducerMap<AppState> = {
    websites: websiteReducer
};

export const effects: Array<any> = [
    WebsiteEffects
];

export const selectWebsites = (appState: AppState) => appState.websites.websites;

export const selectBookmark = createSelector(
    selectWebsites, (websites: websiteState.Website[]) => {
        const bookmarks = Object.keys(groupBy(websites, 'group'));
        return ['All'].concat(bookmarks);
    }
);

export const selectWebsitesByBookmark = (bookmark: string) => createSelector(
    selectWebsites, (websites: websiteState.Website[]) => {
        return websites.filter(website => website.group === bookmark);
    }
);

function groupBy(objectArray, property) {
    return objectArray.reduce(function (s, obj) {
        const key = obj[property];
        if (!s[key]) {
            s[key] = [];
        }
        s[key].push(obj);
        return s;
    }, {});
}
