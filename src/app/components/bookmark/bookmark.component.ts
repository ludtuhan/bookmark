import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { ViewBookmarkComponent } from '../view-bookmark/view-bookmark.component';
import { AddBookmarkComponent } from '../add-bookmark/add-bookmark.component';
import { UpdateBookmarkComponent } from '../update-bookmark/update-bookmark.component';
import { AppState, selectWebsites, selectBookmark, selectWebsitesByBookmark } from '../../store/state/app.state'; 
import { Website } from '../../store/state/website.state';
import { LoadWebsiteInit, DeleteWebsite } from '../../store/actions/website.actions';
import { NotificationService } from '../../services/notification.service';

@Component({
    selector: 'bookmark',
    styleUrls: ['bookmark.component.scss'],
    templateUrl: 'bookmark.component.html',
})
export class BookmarkComponent {
    bookmarkTableConfig;
    addPopupConfig;
    activeFolder;
    bookmarks$: Observable<string[]>;
    bookmarks: string[];
    websites$: Observable<Website[]>;
    websites: Website[];
    selectedBookmark: string;

    constructor(private store: Store<AppState>) {

    }

	ngOnInit() {
	    this.addPopupConfig = {
			type: 'template',
			attribute: 'mat-mini-fab',
			fontIconColor: 'primary',
			fontIcon:'add',
			tooltip:'Add Bookmark',
			template: AddBookmarkComponent
		}

		this.loadData();
		
		this.bookmarkTableConfig = {
			data: this.websites,
			displayedColumns: ['name', 'url','action'],
			columns: [{
				header: "Name",
				field: "name"
			},{
				header: "URL",
				field: "url"
			}],
			viewPopupConfig: {
				type: 'template',
				attribute: 'mat-icon-button',
				fontIconColor: 'primary',
				fontIcon:'visibility',
				tooltip:'View Bookmark',
				template: ViewBookmarkComponent
			},
			editPopupConfig: {
				type: 'template',
				attribute: 'mat-icon-button',
				fontIconColor: 'primary',
				fontIcon:'edit',
				tooltip:'Edit Bookmark',
				template: UpdateBookmarkComponent
			},
			deletePopupConfig: {
				type: 'message',
				attribute: 'mat-icon-button',
				fontIconColor: 'warn',
				fontIcon:'delete',
				tooltip:'Delete Bookmark',
			    messageConfig: {
				    message: 'Are you sure you want to remove this website ?',
                    primaryButtonEnabled: true,
                    primaryButtonLabel: 'Yes',
                    secondaryButtonEnabled: true,
                    secondaryButtonLabel: 'No',
					primaryButtonCallback: function(store: Store<AppState>, notificationService: NotificationService, data: any) {
	                    store.dispatch(new DeleteWebsite(data));
					    notificationService.showNotification({
						    duration: 2000,
						    vPos: 'top',
						    hPos: 'center',
						    message: 'Bookmark was deleted successfully.'
					    });
                    }
				}
			}
		}
	}

	isActive(bookmark: string) {
	    return bookmark === this.activeFolder;
	}

    private loadData() {
	    this.bookmarks$ = this.store.pipe(select(selectBookmark));
        this.bookmarks$.subscribe(response => {
		    this.bookmarks=response;
        });

        this.loadWebsites();
        this.loadBookmark('All');
        this.activeFolder='All';
        
    }

	private loadWebsites() {
		    this.store.dispatch(new LoadWebsiteInit(null));
	}

    loadBookmark(bookmark : string) {
	    this.selectedBookmark = bookmark;
        if(bookmark === 'All') {
	        this.websites$ = this.store.pipe(select(selectWebsites));
        } else {
	        this.websites$ = this.store.pipe(select(selectWebsitesByBookmark(bookmark)));
        }

        this.websites$.subscribe(response => {
		    this.websites=response;

			if(this.bookmarkTableConfig) {
				this.bookmarkTableConfig.data=this.websites;
			}
        });

        this.activeFolder = bookmark;
    }

    
}
