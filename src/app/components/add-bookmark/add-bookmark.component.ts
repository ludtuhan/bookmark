import {Component, ViewChild} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/state/app.state';
import { Website } from '../../store/state/website.state';
import { AddWebsite } from '../../store/actions/website.actions';
import { NotificationService } from '../../services/notification.service';

@Component({
    styleUrls: ['add-bookmark.component.scss'],
    templateUrl: 'add-bookmark.component.html',
})
export class AddBookmarkComponent {
	@ViewChild('addCloseButton', null) addCloseButton;
	
    constructor(private store: Store<AppState>, private notificationService: NotificationService) {
        
    }

    website : Website;
    name: FormControl;
	url: FormControl;
	group: FormControl;

    ngOnInit() {
	    this.name = new FormControl('', [
	        Validators.required
	    ]);

	    this.url = new FormControl('', [
	        Validators.required,
            
	    ]);

	    this.group = new FormControl('', [
	        Validators.required,
            Validators.maxLength(10)
	    ]);
	}

    addBookmark() {
	    if(this.name.hasError('required')) {
		    return false;
	    }

		if(this.url.hasError('required')) {
		    return false;
	    }

		if(this.group.hasError('required') || this.group.hasError('maxlength')) {
		    return false;
	    }

		let name = this.name.value;
		let url = this.url.value;
		let group = this.group.value;

	    this.website = {
		    id: new Date().getTime(), name, url, group
	    }

		this.store.dispatch(new AddWebsite(this.website));
		
		this.addCloseButton._elementRef.nativeElement.click();
		
		this.notificationService.showNotification({
			duration: 2000,
			vPos: 'top',
			hPos: 'center',
			message: 'Website was added successfully!'
		});
    }
}
