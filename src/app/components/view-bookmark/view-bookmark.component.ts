import { Component, Inject, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
    styleUrls: ['view-bookmark.component.scss'],
    templateUrl: 'view-bookmark.component.html',
})
export class ViewBookmarkComponent {
	@ViewChild('viewCloseButton', null)viewCloseButton;
	
    constructor(@Inject(MAT_DIALOG_DATA) public data: any) {
        
    }

    name: FormControl;
	url: FormControl;
	group: FormControl;

    ngOnInit() {
	    this.name = new FormControl(this.data.name, [
	    ]);

	    this.url = new FormControl(this.data.url, [
	     ]);

        this.group = new FormControl(this.data.group, [
	    ]);
	}

}
