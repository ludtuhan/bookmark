import { Component, Inject, ViewChild } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/state/app.state';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NotificationService } from '../../services/notification.service';
import { Website } from '../../store/state/website.state';
import { UpdateWebsite } from '../../store/actions/website.actions';


@Component({
    styleUrls: ['update-bookmark.component.scss'],
    templateUrl: 'update-bookmark.component.html',
})
export class UpdateBookmarkComponent {
	@ViewChild('updateCloseButton', null) updateCloseButton;
	
    constructor(private store: Store<AppState>, @Inject(MAT_DIALOG_DATA) public data: any, private notificationService: NotificationService) {
        
    }
	
	website : Website;
    name: FormControl;
	url: FormControl;
	group: FormControl;
	
    ngOnInit() {
	    this.name = new FormControl(this.data.name, [
	        Validators.required
	    ]);

	    this.url = new FormControl(this.data.url, [
	        Validators.required
	    ]);

        this.group = new FormControl(this.data.group, [
	        Validators.required,
            Validators.maxLength(10)
	    ]);
	}

	updateBookmark() {
	    if(this.name.hasError('required')) {
		    return false;
	    }

		if(this.url.hasError('required')) {
		    return false;
	    }

		if(this.group.hasError('required') || this.group.hasError('maxlength')) {
		    return false;
	    }

		let name = this.name.value;
		let url = this.url.value;
		let group = this.group.value;

	    this.website = {
		    id: this.data.id, name, url, group
	    }

		this.store.dispatch(new UpdateWebsite(this.website));

		this.updateCloseButton._elementRef.nativeElement.click();
		
		this.notificationService.showNotification({
			duration: 2000,
			vPos: 'top',
			hPos: 'center',
			message: 'Bookmark was updated successfully!'
		});
    }
}
