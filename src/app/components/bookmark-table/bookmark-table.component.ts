import { Component, Input } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/state/app.state';
import { NotificationService } from '../../services/notification.service';

interface BookmarkTableConfig {
	columns: any[]
	displayedColumns: string[]
    tooltip: any
    data: any
    viewPopupConfig: any
    editPopupConfig: any
    deletePopupConfig: any
    deleteFunc: Function
}

@Component({
	selector: 'bookmark-table',
    styleUrls: ['bookmark-table.component.scss'],
    templateUrl: 'bookmark-table.component.html',
})
export class BookmarkTableComponent {
	
    constructor(private store: Store<AppState>, private notificationService: NotificationService) {
        
    }

    @Input() config : BookmarkTableConfig;

	delete(data: any) {
		this.config.deleteFunc(data, this.store, this.notificationService);
	}
}
