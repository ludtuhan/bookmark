import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Websites } from '../store/state/website.state';

@Injectable({providedIn: 'root'})
export class WebsiteService {

    private dummyWebsites: Websites = {
        websites: [
            {id:1, name: 'Facebook', url: 'https:www.facebook.com', group: 'Personal'},
            {id:2, name: 'Cebu Pacific', url: 'https://www.cebupacificair.com/', group: 'Leisure'},
 			{id:3, name: 'LinkedIn', url: 'https://www.linkedin.com/', group: 'Work'}
            
        ]
    };

    getWebsites(): Observable<Websites>{
        return new Observable(observer => {
            observer.next(this.dummyWebsites);
            observer.complete();
        });
    }
}
